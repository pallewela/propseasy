/*
 * Copyright (c) 2013, Dumindu Pallewela
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */

package org.pallewela.dynaprops;

import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.pallewela.dynaprops.DynamicProperties.DefaultValue;

/**
 * @author Dumindu Pallewela
 */
public class AnnotatedClassPropertiesLoader implements PropertiesLoader {
    private static final Logger LOGGER = Logger.getLogger(AnnotatedClassPropertiesLoader.class.getCanonicalName());

    @Override
    public <T extends DynamicProperties> void load(Config<T> config, PropertiesCache cache) {
        Class<T> propsClass = config.getPropertiesClass();
        for (Method method : propsClass.getDeclaredMethods()) {
            DefaultValue defaultValueAnnotation = method.getAnnotation(DefaultValue.class);

            if (defaultValueAnnotation != null) {
                String defaultValue = defaultValueAnnotation.value();
                try {
                    cache.put(method, defaultValue);
                } catch (PropertyNotExistException ex) {
                    LOGGER.log(Level.SEVERE, "Error in PropertiesCache.java", ex);
                    throw new RuntimeException("Error in PropertiesCache.java", ex);
                }
            }
        }
    }
}
