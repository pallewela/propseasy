/*
 * Copyright (c) 2013, Dumindu Pallewela
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */

package org.pallewela.dynaprops;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Dumindu Pallewela
 */
class Config<T extends DynamicProperties> {
    private List<DynamicProperties.DataSource.Type> dataSourceTypes = new ArrayList<DynamicProperties.DataSource.Type>(Arrays.asList(DynamicProperties.DataSource.Type.ANNOTATED_CLASS));
    private Class<T> propertiesClass;
    private String propertiesFile;
    private MethodPropertyMapper methodPropertyMapper;
    private boolean useJmx = false;
    private String jmxObjectName;

//    MethodNameToKeyFormatter getMethodNameToKeyFormatter() {
//        return formatter;
//    }
//
//    //TODO: remove
//    void setMethodNameToKeyFormatter(final MethodNameToKeyFormatter formatter) {
//        this.formatter = formatter;
//    }

    public void setDataSourceTypes(List<DynamicProperties.DataSource.Type> dataSourceTypes) {
        for (DynamicProperties.DataSource.Type dsType : dataSourceTypes) {
            this.dataSourceTypes.add(dsType);
        }
    }

    public List<DynamicProperties.DataSource.Type> getDataSourceTypes() {
        return dataSourceTypes;
    }

    public void setPropertiesClass(Class<T> propertiesClass) {
        this.propertiesClass = propertiesClass;
    }

    public Class<T> getPropertiesClass() {
        return propertiesClass;
    }

    public String getPropertiesFile() {
        return propertiesFile;
    }

    public void setPropertiesFile(String propertiesFile) {
        this.propertiesFile = propertiesFile;
    }

    public void setMethodPropertyMapper(MethodPropertyMapper methodPropertyMapper) {
        this.methodPropertyMapper = methodPropertyMapper;
    }

    public MethodPropertyMapper getMethodPropertyMapper() {
        return methodPropertyMapper;
    }

    boolean useJmx() {
        return useJmx;
    }

    void setUseJmx(boolean useJmx) {
        this.useJmx = useJmx;
    }

    String getJmxObjectName() {
        return jmxObjectName;
    }

    void setJmxObjectName(String jmxObjectName) {
        this.jmxObjectName = jmxObjectName;
    }
}
