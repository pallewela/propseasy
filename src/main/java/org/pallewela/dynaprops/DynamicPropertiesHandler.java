/*
 * Copyright (c) 2013, Dumindu Pallewela
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */

package org.pallewela.dynaprops;

import javax.management.JMException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author Dumindu Pallewela
 */
public class DynamicPropertiesHandler<T extends DynamicProperties> implements InvocationHandler {
    private PropertiesCache cache;

    // This is where we initialize everything.
    public DynamicPropertiesHandler(Class<T> clazz) throws JMException {
        Config<T> config = DynamicPropertiesUtil.createConfig(clazz);
        cache = new PropertiesCache(config.getMethodPropertyMapper());
        DynamicPropertiesUtil.load(config, cache);

        if (config.useJmx()) {
	    //TODO: get the jmxport from config and use it. if it's not set
	    // use ManagementFactory.getPlatformMBeanServer() instead.
            //MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            JmxConnectorServer mbs = new JmxConnectorServer(9797);
            DynamicPropertiesMBean mbean = new DynamicPropertiesMBean(config, cache);
            mbs.registerMBean(mbean, new ObjectName(config.getJmxObjectName()));
        }
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return cache.get(method);
    }
}
