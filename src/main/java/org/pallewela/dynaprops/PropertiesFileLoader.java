/*
 * Copyright (c) 2013, Dumindu Pallewela
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */

package org.pallewela.dynaprops;

import org.pallewela.dynaprops.util.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Dumindu Pallewela
 */
public class PropertiesFileLoader implements PropertiesLoader {
    private static final Logger LOGGER = Logger.getLogger(PropertiesFileLoader.class.getName());

    @Override
    public <T extends DynamicProperties> void load(Config<T> config, PropertiesCache cache) {
        LOGGER.log(Level.FINE, "Loading properties from file: %s", config.getPropertiesFile());
        Properties props = new Properties();
        if (Utils.isNotNullOrEmpty(config.getPropertiesFile())) {
            InputStream in = this.getClass().getClassLoader().getResourceAsStream(config.getPropertiesFile());

            if (in != null) {
                try {
                    props.load(in);
                    LOGGER.log(Level.FINE, "Successfully read properties from file: %s", config.getPropertiesFile());

                    for (String propertyName : props.stringPropertyNames()) {
                        try {
                            cache.putWithPropertyName(propertyName, props.getProperty(propertyName));
                        } catch (PropertyNotExistException e) {
                            LOGGER.log(Level.WARNING,
                                    "A method mapped with %s property doesn't exist in class %s",
                                    new String[]{propertyName, config.getPropertiesClass().getCanonicalName()});
                        }
                    }

                } catch (IOException e) {
                    LOGGER.log(Level.WARNING, "Failed loading properties from file: %s", config.getPropertiesFile());
                }
            }
        } else {
            LOGGER.log(Level.WARNING, "Properties file name is empty.");
        }
    }
}
