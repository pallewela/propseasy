/*
 * Copyright (c) 2013, Dumindu Pallewela
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */

package org.pallewela.dynaprops;

import org.pallewela.dynaprops.util.Utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.pallewela.dynaprops.DynamicProperties.*;

/**
 * @author Dumindu Pallewela
 */
public class DynamicPropertiesUtil {
    private DynamicPropertiesUtil() {
    }

    private static final Logger LOGGER = Logger.getLogger(DynamicPropertiesUtil.class.getCanonicalName());

    public static <T extends DynamicProperties> void load(Config<T> config, PropertiesCache cache) {
        for (DataSource.Type dsType : config.getDataSourceTypes()) {
            PropertiesLoader loader;
            switch (dsType) {
                case ANNOTATED_CLASS:
                    loader = new AnnotatedClassPropertiesLoader();
                    break;
                case PROPERTIES_FILE:
                    loader = new PropertiesFileLoader();
                    break;
                default:
                    loader = new DummyPropertiesLoader();
                    LOGGER.log(Level.WARNING, "Unsupported DataSource type.");
            }

            loader.load(config, cache);

        }
    }

    static <T extends DynamicProperties> Config<T> createConfig(Class<T> clazz) {
        Config<T> config = new Config<T>();

        config.setPropertiesClass(clazz);


        // Find method name to key name formatters. If not set use SimpleMethodNameToKeyFormatter as default
        MethodNameToKeyFormatter formatter = new SimpleMethodNameToKeyFormatter();
        KeyFormatter formatterAnnotation = clazz.getAnnotation(KeyFormatter.class);
        if (formatterAnnotation != null) {
            try {
                formatter = formatterAnnotation.value().newInstance();
            } catch (ReflectiveOperationException e) {
                LOGGER.log(Level.WARNING, "Failed to instantiate formatter: %s", formatterAnnotation.value().getCanonicalName());
            }
        }

        //Create the MethodPropertyMapper based on the selected MethodNameToKeyFormatter.
        config.setMethodPropertyMapper(new MethodPropertyMapper(clazz, formatter));


        // Find data sources
        DataSource dataSourceAnnotation = clazz.getAnnotation(DataSource.class);
        if (dataSourceAnnotation != null) {
            List<DataSource.Type> list = Arrays.asList(dataSourceAnnotation.type());
            Collections.reverse(list); //Reverse the order so that DataSources specified first are given higher priority.
            config.setDataSourceTypes(list);

            if (config.getDataSourceTypes().contains(DataSource.Type.PROPERTIES_FILE)) {
                String propsFile = dataSourceAnnotation.propertiesFile();
                if (Utils.isNotNullOrEmpty(propsFile)) {
                    config.setPropertiesFile(propsFile);
                } else {
                    LOGGER.log(Level.WARNING, "Properties file name is empty.");
                }
            }
        }

        // Find JMX related configuration
        UseJMX jmxAnnotation = clazz.getAnnotation(UseJMX.class);
        if (jmxAnnotation != null) {
            config.setUseJmx(true);
            String objectName = jmxAnnotation.objectName();
            if ("__DEFAULT__".compareTo(objectName) == 0) {
                config.setJmxObjectName(generateDefaultObjectName(clazz));
            } else {
                config.setJmxObjectName(objectName);
            }
        } else {
            config.setUseJmx(false);
        }

        return config;
    }

    //generate object name of format: "package.name:type=ClassName"
    private static <T extends DynamicProperties> String generateDefaultObjectName(Class<T> clazz) {
        return clazz.getPackage().getName() + ":type=" + clazz.getSimpleName();
    }
}
