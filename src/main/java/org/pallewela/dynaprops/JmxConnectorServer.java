package org.pallewela.dynaprops;

import javax.management.*;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.rmi.registry.LocateRegistry;

public class JmxConnectorServer {
    private int serverPort;
    private int rmiPort;
    private MBeanServer mbeanServer;

    public JmxConnectorServer(int serverPort) throws JMException {
        this(serverPort, serverPort+1);
    }

    public JmxConnectorServer(int serverPort, int rmiPort) throws JMException {
        this.serverPort = serverPort;
        this.rmiPort = rmiPort;

        this.start();
    }

    public void registerMBean(Object obj, ObjectName objName) throws NotCompliantMBeanException, InstanceAlreadyExistsException, MBeanRegistrationException {
        mbeanServer.registerMBean(obj, objName);
    }

    private synchronized void start() throws JMException {
        JMXConnectorServer connServer;
        try {
            LocateRegistry.createRegistry(rmiPort);
            JMXServiceURL url;
            url = new JMXServiceURL("service:jmx:rmi://localhost:" + serverPort + "/jndi/rmi://:" + rmiPort + "/jmxrmi");
            //TODO: remove
            System.out.println(url.toString());
            connServer = JMXConnectorServerFactory.newJMXConnectorServer(url, null, ManagementFactory.getPlatformMBeanServer());
            connServer.start();
        } catch (IOException e) {
            throw newJMException("Failed creating JMXConnector server", e);
        }

        this.mbeanServer = connServer.getMBeanServer();
    }

    private JMException newJMException(String msg, Throwable cause) {
        JMException ex = new JMException(msg);
        ex.initCause(cause);
        return ex;
    }
}
