/*
 * Copyright (c) 2013, Dumindu Pallewela
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */

package org.pallewela.dynaprops;

import org.pallewela.dynaprops.util.Utils;

import javax.management.*;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Dumindu Pallewela
 */
public class DynamicPropertiesMBean implements DynamicMBean {
    private static Logger LOGGER = Logger.getLogger(DynamicPropertiesMBean.class.getCanonicalName());
    private Config<? extends DynamicProperties> config;
    private PropertiesCache cache;

    public DynamicPropertiesMBean(Config<? extends DynamicProperties> config, PropertiesCache cache) {
        this.config = config;
        this.cache = cache;
    }

    @Override
    public Object getAttribute(String attribute) throws AttributeNotFoundException, MBeanException, ReflectionException {
        try {
            return new Attribute(attribute, cache.get(attribute));
        } catch (PropertyNotExistException ex) {
            throw createAttrNotFoundEx("Attribute " + attribute + " not found", ex);
        }
    }

    @Override
    public void setAttribute(Attribute attribute) throws AttributeNotFoundException, InvalidAttributeValueException, MBeanException, ReflectionException {
        if (attribute == null) throw new AttributeNotFoundException("attribute parameter should not be null");

        if (attribute.getName() != null) {
            String value;
            if (attribute.getValue() != null) {
                value = attribute.getValue().toString();
            } else {
                throw new InvalidAttributeValueException("Attribute value should not be null");
            }
            try {
                cache.put(attribute.getName(), value);
            } catch (PropertyNotExistException ex) {
                throw createAttrNotFoundEx("Attribute " + attribute.getName() + " not found", ex);
            }
        } else {
            throw new AttributeNotFoundException("Attribute name should not be null");
        }
    }

    @Override
    public AttributeList getAttributes(String[] attributes) {
        AttributeList list = new AttributeList();
        if (attributes != null) {
            for (String attribute : attributes) {
                try {
                    list.add(getAttribute(attribute));
                } catch (JMException e) {
                    LOGGER.log(Level.FINE, "Unable to get attribute %s", attribute);
                }
            }
        } else {
            LOGGER.log(Level.FINE, "attributes parameter should not be null");
        }

        return list;
    }

    @Override
    public AttributeList setAttributes(AttributeList attributes) {
        AttributeList attrsSet = new AttributeList();
        if (attributes != null) {
            for (Attribute attribute : attributes.asList()) {
                try {
                    setAttribute(attribute);
                    attrsSet.add(attribute);
                } catch (JMException e) {
                    LOGGER.log(Level.FINE, "Failed setting attribute %s : %s",
                            new String[]{attribute.toString(), Utils.getExceptionDetails(e)});
                }
            }
        } else {
            LOGGER.log(Level.FINE, "attributes parameter should not be null");
        }
        return attrsSet;
    }

    @Override
    public Object invoke(String actionName, Object[] params, String[] signature) throws MBeanException, ReflectionException {
        throw new MBeanException(new IllegalStateException("Unsupported"));
    }

    @Override
    public MBeanInfo getMBeanInfo() {
        Method[] methods = config.getPropertiesClass().getDeclaredMethods();
        MBeanAttributeInfo[] attrs = new MBeanAttributeInfo[methods.length];

        int i = 0;
        for (Method method : methods) {
            boolean isWritable = method.getAnnotation(DynamicProperties.ReadOnly.class) == null;
            attrs[i++] = new MBeanAttributeInfo(
                    method.getName(),
                    "java.lang.String",
                    "Property " + config.getMethodPropertyMapper().map(method),
                    true,   // isReadable
                    isWritable,   // isWritable
                    false); // isIs
        }

        return new MBeanInfo(
                this.getClass().getName(),
                "Property Manager MBean",
                attrs,
                null,  // constructors
                null,
                null); // notifications
    }

    private AttributeNotFoundException createAttrNotFoundEx(String message, Throwable cause) {
        AttributeNotFoundException ex = new AttributeNotFoundException(message);
        ex.initCause(cause);
        return ex;
    }
}
