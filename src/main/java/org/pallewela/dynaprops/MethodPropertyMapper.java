/*
 * Copyright (c) 2013, Dumindu Pallewela
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */

package org.pallewela.dynaprops;

import org.pallewela.dynaprops.util.Utils;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Dumindu Pallewela
 */
public class MethodPropertyMapper {
    MethodNameToKeyFormatter formatter;
    Map<String, String> mapping = new HashMap<String, String>();

    public <T extends DynamicProperties> MethodPropertyMapper(Class<T> clazz, MethodNameToKeyFormatter formatter) {
        this.formatter = formatter;

        for (Method method : clazz.getDeclaredMethods()) {
            DynamicProperties.Key keyAnnotation = method.getAnnotation(DynamicProperties.Key.class);
            String key;
            if (keyAnnotation != null && Utils.isNotNullOrEmpty(keyAnnotation.value())) {
                key = keyAnnotation.value();
            } else {
                key = formatter.format(method.getName());
            }
            mapping.put(method.getName(), key);
        }
    }

    public boolean isPropertyAvailable(String methodName) {
        return mapping.values().contains(methodName);
    }

    public String map(Method method) {
        return map(method.getName());
    }

    public String map(String methodName) {
        return mapping.get(methodName);
    }
}
