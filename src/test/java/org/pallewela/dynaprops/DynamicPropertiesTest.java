/*
 * Copyright (c) 2013, Dumindu Pallewela
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */

package org.pallewela.dynaprops;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author Dumindu Pallewela
 */
public class DynamicPropertiesTest {
    @BeforeMethod
    public void setUp() throws Exception {
    }

    @Test
    public void testMethod() throws DynamicPropertiesCreationException {
        Assert.assertEquals(true, true);
        SampleProperties props = DynamicPropertiesFactory.create(SampleProperties.class);
        System.out.println("props.property1() : " + props.property1());
        System.out.println("props.property2() : " + props.property2());
        System.out.println("props.property3() : " + props.property3());
        System.out.println("props.property4() : " + props.property4());
        System.out.println("sillyPropertyNameToCheckCamelCase() :" + props.sillyPropertyNameToCheckCamelCase());
        Assert.assertEquals(props.property2(), "prop2.file.value", "DefaultValue value for property3 was not loaded");
        Assert.assertEquals(props.property3(), "prop3.default.value", "DefaultValue value for property3 was not loaded");
        Assert.assertEquals(props.property4(), "prop4.default.value", "DefaultValue value for property4 was not loaded");
    }
}